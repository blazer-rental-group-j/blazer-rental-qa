package agilegroupj.blazerrental;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.edge.EdgeOptions;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
public class TestingQA {
    WebDriver driver;

    @BeforeTest
    public void setUp() {
        System.setProperty("webdriver.edge.driver",
                "C:\\Users\\USER\\Downloads\\msedgedriver.exe");
        
        EdgeOptions options = new EdgeOptions();
        options.addArguments("--remote-allow-origins=*");
        driver = new EdgeDriver(options);
        driver.get("https://www.figma.com/file/JovJgTq7Kuc9x6VTIoXYg9/Blazer-Rental-Flow-Chart-and-ER-Diagram?type=whiteboard&node-id=41-852&t=Vk7PkxGJ8XzPyXlI-0");
    }

    @AfterTest
    public void tearDown() {
        driver.quit();
    }

    @Test
    public void testPositiveScenario() {

    }
    @Test
    public void testNegativeScenario1() {
    }

    @Test
    public void testNegativeScenario2() {

    }
}